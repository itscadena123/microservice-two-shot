import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function ShoeForm({ getShoes }) {
    const navigate = useNavigate();
    const [manufacturer, setManufacturer] = useState('');
    const [modelName, setModelName] = useState('');
    const [color, setColor] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [bin, setBin] = useState('');
    const [bins, setBins] = useState([]);

    async function fetchBins() {
      const url = 'http://localhost:8100/api/bins/';

      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setBins(data.bins);
      }
    }

    useEffect(() => {
      fetchBins();
    }, [])

    async function handleSubmit(event) {
      event.preventDefault();
      const data = {
        manufacturer,
        model_name: modelName,
        color,
        picture_url: pictureUrl,
        bin,
      };

      const shoesUrl = 'http://localhost:8080/api/shoes/';
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const response = await fetch(shoesUrl, fetchConfig);
      if (response.ok) {
        const newShoe = await response.json();
        console.log(newShoe);
        setManufacturer('');
        setModelName('');
        setColor('');
        setPictureUrl('');
        setBin('');
        getShoes();
        navigate('/shoes/')
      }
    }

    function handleChangeManufacturer(event) {
      const { value } = event.target;
      setManufacturer(value);
    }

    function handleChangeModelName(event) {
      const { value } = event.target;
      setModelName(value);
    }

    function handleChangeColor(event) {
      const { value } = event.target;
      setColor(value);
    }

    function handleChangePictureUrl(event) {
      const { value } = event.target;
      setPictureUrl(value);
    }

    function handleChangeBin(event) {
      const { value } = event.target;
      setBin(value);
    }

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a shoe</h1>
            <form onSubmit={handleSubmit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input value={manufacturer} onChange={handleChangeManufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input value={modelName} onChange={handleChangeModelName} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control" />
                <label htmlFor="model_name">Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={color} onChange={handleChangeColor} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input value={pictureUrl} onChange={handleChangePictureUrl} placeholder="Picture URL" required type="url" name="picture_url" id="picture_url" className="form-control" />
                <label htmlFor="picture_url">Picture URL</label>
              </div>
              <div className="mb-3">
                <select value={bin} onChange={handleChangeBin} required name="bin" id="bin" className="form-select">
                  <option value="">Choose a bin</option>
                  {bins.map(bin => {
                    return (
                      <option key={bin.id} value={bin.id}>{bin.closet_name}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Add Shoe</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default ShoeForm;
