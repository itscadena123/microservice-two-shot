import React, { useState, useEffect } from "react";


function HatList() {
    const [hats, setHats] = useState([]);

    const handleDeleteHat = (id) => {
        if (window.confirm("Confirm Delete?")) {
            fetch(`http://localhost:8090/api/hats/${id}/`, {method: "DELETE" }).then(
                () => {
                    window.location.reload();
                }
            );
        }
    }

    const fetchData = async () => {
        const response = await fetch ('http://localhost:8090/api/hats/');
        if (response.ok) {
            const data = await response.json();
            setHats(data.hats);
        }
    }

    useEffect(()=>{
        fetchData()
    }, [])

    return (
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Fabric</th>
              <th>Style Name</th>
              <th>Color</th>
              <th>Image Url</th>
              <th>Location</th>
            </tr>
          </thead>
          <tbody>
            {hats.map(hat => {
              return (
                <tr key={hat.id}>
                  <td>{ hat.fabric }</td>
                  <td>{ hat.style_name }</td>
                  <td>{ hat.color }</td>
                  <td> <img src={hat.picture_url} alt="" width="100" /></td>
                  <td>{ hat.location }</td>
                  <td>
                    <button onClick={() => handleDeleteHat(hat.id)}>Delete Hat</button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      );
}

export default HatList;
